 2002  git clone git@gitlab.com:danielpadrta/example.git
 2003  ls
 2004  cd example/
 2005  ls
 2006  git pull
 2007  ls
 2008  vim README.md 
 2009  git status
 2010  git add README.md 
 2011  git commit -m 'Update README to describe the project'
 2012  git push
 2013  history | less
 2014  vim 524649_history.md
 2015  git add 524649_history.md 
 2016  git commit -m 'Add history'
 2017  git push
 2018  git branch 'meaningful_change'
 2019  git switch meaningful_change 
 2020  vim README.md 
 2021  git status
 2022  git add README.md 
 2023  git commit -m 'Update README to include a meaningful change'
 2024  git push
 2025  git push --set-upstream origin meaningful_change
